package be.ordina;

import be.ordina.pages.HomePage;
import be.ordina.pages.PlexApplication;
import be.ordina.pages.SignInFrame;
import be.ordina.system.Site;
import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import static org.junit.Assert.assertTrue;

@RunWith(DataProviderRunner.class)
public class AppTest3Logins {
    static File FileName = new File("TestDataFiles\\DataPlexTesten.xlsx");
    public static DataFormatter formatter = new DataFormatter();

    private WebDriver driver;

    @Before
    public void setUp(){

        driver = Site.openPlexChrome(driver);
    }

    @Ignore
    @Test
    public void shouldAnswerWithTrue()
    {

        assertTrue( true );
    }

    @Ignore
    @DataProvider
    public static Object[][] provideLogin() throws IOException {
        FileInputStream file = new FileInputStream(FileName);
        XSSFWorkbook workbook = new XSSFWorkbook(file);
        XSSFSheet sheet = workbook.getSheet("Logins");

        int RowNum = sheet.getPhysicalNumberOfRows();
        int ColNum = sheet.getRow(0).getPhysicalNumberOfCells();

        System.out.println("Number of rows:" + RowNum + " number of columns: " + ColNum);
        String[][] xlData = new String[RowNum-1][ColNum-1];

        for (int i = 0; i < RowNum - 1; i++)
        {
            XSSFRow row = sheet.getRow(i + 1);
            for (int j = 0; j < ColNum-1; j++)
            {
                if (row == null)
                    xlData[i][j] = "";
                else {
                    XSSFCell cell = row.getCell(j);
                    if (cell == null)
                        xlData[i][j] = "";
                    else {
                        String value = formatter.formatCellValue(cell);
                        xlData[i][j] = value.trim();
                    }
                }
            }
        }
        return xlData;

    }

    @Ignore
    @Test
    @UseDataProvider("provideLogin")
    public void Login (String username, String email, String password) throws InterruptedException {
        HomePage homePage = new HomePage(driver);
        homePage.clickSignIn();

        SignInFrame signInframe = new SignInFrame(driver);
        signInframe.signIn(username, email, password);


        homePage.launch();

        PlexApplication plexApplication = new PlexApplication(driver);
        plexApplication.checkPlexOverview();

        plexApplication.menuLogout();
    }

/*
    @After
    public void Close(){
        driver.close();
    }
*/
}
