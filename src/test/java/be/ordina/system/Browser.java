package be.ordina.system;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class Browser {
    public static WebDriver chrome(){
        System.setProperty("webdriver.chrome.driver", "C:\\Users\\pewa\\Documents\\Opleiding Testautomation\\drivers\\chromedriver.exe");
        return new ChromeDriver();
    }

    public static WebDriver firefox(){
        System.setProperty("webdriver.firefox.driver","C:\\Users\\pewa\\Documents\\Opleiding Testautomation\\drivers\\geckodriver.exe");
        return new FirefoxDriver();
    }

    public static WebDriver edgedriver(){
        System.setProperty("webdriver.edge.driver","C:\\Users\\pewa\\Documents\\Opleiding Testautomation\\drivers\\msedgedriver.exe");
        return new EdgeDriver();

    }
}
