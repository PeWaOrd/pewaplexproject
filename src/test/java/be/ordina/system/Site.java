package be.ordina.system;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

public class Site {

    private WebDriver driver;

    public Site() {
        this.driver = driver;
    }

    public static WebDriver openPlexChrome(WebDriver driver){

        driver = Browser.chrome();
        driver.get("https://www.plex.tv/");
        driver.manage().window().maximize();

        return driver;

    }

    public static WebDriver openPlexFirefox(WebDriver driver){
        driver = Browser.firefox();
        driver.get("https://www.plex.tv/");
        driver.manage().window().maximize();

        return driver;
    }

    public static WebDriver openEdge(WebDriver driver){
        driver = Browser.edgedriver();
        driver.get("https://www.plex.tv/");
        driver.manage().window().maximize();

        return driver;
    }
}
