package be.ordina;

import be.ordina.general.MainUser;
import be.ordina.general.User;
import be.ordina.pages.*;
import be.ordina.system.Site;
import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.*;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

@RunWith(DataProviderRunner.class)
public class AppTest2Categorie {
    private static WebDriver driver;
    private static PlexApplication plexApplication;
    static File FileName = new File("TestDataFiles\\DataPlexTesten.xlsx");
    public static DataFormatter formatterCheckCategorie = new DataFormatter();


    @BeforeClass
    public static void InitBeforeAllTests() throws InterruptedException {
    //    driver = Site.openPlexChrome(driver);
        driver = Site.openEdge(driver);
        SignInGeneralAndLaunch();
        PlexApplicationControl();
    }

    @Before
    public void InitBeforeTest(){

    }

    @DataProvider
    public static Object[][] CheckCategorieData () throws IOException {
        FileInputStream file = new FileInputStream(FileName);
        XSSFWorkbook workbook = new XSSFWorkbook(file);
        XSSFSheet sheet = workbook.getSheet("CategorieTesten");

        int RowNum = sheet.getPhysicalNumberOfRows();
        int ColNum = sheet.getRow(0).getPhysicalNumberOfCells();

        System.out.println("Number of rows:" + RowNum + " number of columns: " + ColNum);
        String[][] xlDataCheckCategorie = new String[RowNum-1][ColNum-1];

        for (int i = 0; i < RowNum - 1; i++)
        {
            XSSFRow row = sheet.getRow(i + 1);
            for (int j = 0; j < ColNum-1; j++)
            {
                if (row == null)
                    xlDataCheckCategorie[i][j] = "";
                else {
                    XSSFCell cell = row.getCell(j);
                    if (cell == null)
                        xlDataCheckCategorie[i][j] = "";
                    else {
                        String value = formatterCheckCategorie.formatCellValue(cell);
                        xlDataCheckCategorie[i][j] = value.trim();
                    }
                }
            }
        }
        return xlDataCheckCategorie;

    }



    @Test
    @UseDataProvider("CheckCategorieData")
    public void checkAllowedMovieCategorieUser(String username, String restriction, String categorie, String maxNrOfMovies){
        SwitchandSelectUser switchandSelectUser = new SwitchandSelectUser(driver);
        SideBarMovieAndShows sideBarMovieAndShows = new SideBarMovieAndShows(driver);
        Categories categories = new Categories(driver,categorie,maxNrOfMovies);

        User user = new User(username);
        switchandSelectUser.SelectUser(user);

        plexApplication.sideBarMovieAndShows();
        sideBarMovieAndShows.selectTabCategorie("CATEGORIES");
        categories.CheckOnCategories();
        categories.SelectCategorie();
        categories.CheckAllowedCategorie();
    }

    @After
    public void FinalAfterTest(){

        plexApplication.menuSwitchUser();
    }

    @AfterClass
    public static void FinalAfterAllTests(){
        SwitchandSelectUser switchandSelectUser = new SwitchandSelectUser(driver);

        User user = new User("Joeri1");
        switchandSelectUser.SelectUser(user);
        plexApplication.menuLogout();

        driver.close();
        System.out.println("End of all tests");
    }


    public static void SignInGeneralAndLaunch() throws InterruptedException {
        HomePage homePage = new HomePage(driver);
        homePage.clickSignIn();

        SignInFrame signInframe = new SignInFrame(driver);
        MainUser mainuser = new MainUser("Joeri1", "managerjoeri@mailinator.com", "Y5.gG&4.F/1A");

        signInframe.signInMainuser(mainuser);

        homePage.launch();
        homePage.selectUser();
    }

    public static void PlexApplicationControl(){
        plexApplication = new PlexApplication(driver);
        plexApplication.checkPlexOverview();

        plexApplication.menuSwitchUser();
    }

}
