package be.ordina;

import static org.junit.Assert.assertTrue;


import be.ordina.general.MainUser;
import be.ordina.general.Movie;
import be.ordina.pages.*;
import be.ordina.system.Site;
import org.junit.After;
import org.junit.Before;
import org.junit.Ignore;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

import java.io.IOException;


/**
 * Unit test for simple App.
 */
public class AppTest
{
    private WebDriver driver;

    @Before
    public void setUp(){

        driver = Site.openPlexChrome(driver);
    }
    /**
     * Rigorous Test :-)
     */

    @Ignore
    @Test
    public void shouldAnswerWithTrue()
    {
        assertTrue( true );
    }


    @Ignore
    @Test
    public void registerNewAccount() throws IOException, InterruptedException {
        HomePage homePage = new HomePage(driver);
        homePage.clickSignUp();

        MainUser mainUser = new MainUser();
        SignUpFrame signUpFrame = new SignUpFrame(driver);
        mainUser = signUpFrame.signup();

        PlexApplication plexApplication = new PlexApplication(driver);
        plexApplication.checkPlexOverview();
        plexApplication.menuNameUser();

        AccountPage accountPage = new AccountPage(driver);
        accountPage.checkAccount();
        accountPage.changeUsername(mainUser.getName());

        driver.navigate().refresh();

    }

    @Ignore
    @Test
    public void signInShowOverviewAllMovies() throws InterruptedException {
        SignInGeneralAndLaunch();

        PlexApplication plexApplication = new PlexApplication(driver);
        plexApplication.checkPlexOverview();

        plexApplication.menuLogout();
 //       driver.close();
    }

    @Ignore
    @Test
    public void signInSearchMovie() throws InterruptedException {
        SignInGeneralAndLaunch();

        PlexApplication plexApplication = new PlexApplication(driver);
        plexApplication.checkPlexOverview();

        Movie movie = new Movie();

        plexApplication.searchMovie(movie.getMovie());
        plexApplication.checkPlexDetail(movie.getMovie());
    }

    @Ignore
    @Test
    public void SigInSearchMovieWatchlistHandle() throws InterruptedException {
        SignInGeneralAndLaunch();

        PlexApplication plexApplication = new PlexApplication(driver);
        plexApplication.checkPlexOverview();

        Movie movie = new Movie();

        plexApplication.searchMovie(movie.getMovie());
        plexApplication.checkPlexDetail(movie.getMovie());

        plexApplication.watchListHandle();
    }

    /*
    @After
    public void close(){
        driver.close();
    }*/


    public void SignInGeneralAndLaunch() throws InterruptedException {
        HomePage homePage = new HomePage(driver);
        homePage.clickSignIn();

        SignInFrame signInframe = new SignInFrame(driver);
        signInframe.signIn("Manager429", "Manager429@mailinator.com", "J3tJ+K=sUf$4");

        homePage.launch();
    }
}
