package be.ordina;

import be.ordina.system.Site;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.openqa.selenium.WebDriver;

public class AppTestBrowsers {
     private WebDriver driver;

     @Before
     public void setUp(){
     }


     @Test
     public void openviaChrome(){
          driver = Site.openPlexChrome(driver);
     }

     @Test
     public void openviaFirefox(){
         driver = Site.openPlexFirefox(driver);
      }

     @Test
     public void openedge() {

          driver = Site.openEdge(driver);
     }

     @After
     public void close(){
     //      driver.close();
     }
}
