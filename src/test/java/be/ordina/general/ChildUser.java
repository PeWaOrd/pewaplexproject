package be.ordina.general;

public class ChildUser extends User {
    private String Profile;

    public ChildUser(String name, String profile) {
        super(name);
        Profile = profile;
    }

    public ChildUser(String name) {
        super(name);
    }

    public String getProfile() {
        return Profile;
    }

    public void setProfile(String profile) {
        Profile = profile;
    }
}
