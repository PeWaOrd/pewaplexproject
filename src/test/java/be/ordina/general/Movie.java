package be.ordina.general;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

// Temp only used in AppTest for exercises
public class Movie {
    private String movie;

    public Movie()  {
        try {
            this.movie = searchMovie();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public String getMovie() {
        return movie;
    }

    public void setMovie(String movie) {
        this.movie = movie;
    }

    public static String searchMovie() throws IOException {
        String searchmovie = "";
 //       String TestFile = "C:\\SearchTemp\\SearchMovie.txt";
       String TestFile = "TestDataFiles\\SearchMovie.txt";

        FileReader FR = new FileReader(TestFile);
        BufferedReader BR = new BufferedReader(FR);
        String Content = "";

        //Loop to read all lines one by one from file and print It.
        while((Content = BR.readLine()) != null){
//            System.out.println(Content);
            searchmovie = Content;
        }
        return searchmovie;
    }
}
