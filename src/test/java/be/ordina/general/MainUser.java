package be.ordina.general;

import java.util.Random;

public class MainUser extends User {
    private String email;
    private String password;

    public MainUser() {
        String[] UserEmail ;

        UserEmail = createEmail();
        this.setName(UserEmail[0]);
        this.email = UserEmail[1];
        this.password = createPassword(10,1,1);
    }

    public MainUser(String name, String email, String password) {
        this.setName(name);
        this.email = email;
        this.password = password;
    }



    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public static String[] createEmail() {
        String[] return_array = new String[2];

        String usernames [] = {"User" , "Admin" , "Tester" ,"Visitor","Manager"};
        String numbers = "0123456789";
        String extension = "@mailinator.com";

        // Chose random User
        Random randomusername = new Random();
        int randomNumber= randomusername.nextInt(usernames.length);

        // Chose random number with lenght of 3
        StringBuilder builderFollowNr = new StringBuilder();
        Random randomFollowNr = new Random();
        for (int i=1;i<=3;i++){
            int index = randomFollowNr.nextInt(numbers.length());

            // get character specified by index from the string numbers
            char randomChar = numbers.charAt(index);

            // append the character to string builder
            builderFollowNr.append(randomChar);
        }

        String Username = usernames[randomNumber];
        return_array[0] = Username + builderFollowNr.toString();
        return_array[1] = Username + builderFollowNr.toString() + extension;
        return return_array;
    }


    public static String createPassword(int lengthchar, int lengthnumber , int lengthspecial){
        String charLower = "abcdefghijklmnopqrstuvwxyz";
        String charUpper = charLower.toUpperCase();
        String numberchar = "0123456789";
        String specialchar = ",?;.:/=+ù%µ£$*";

        String totalchar = charLower + charUpper;
        String allchar = charLower + charUpper + numberchar + specialchar;

        // First character a character from Alphabet , random
        Random randomfirst = new Random();
        int indexfirst = randomfirst.nextInt(totalchar.length());
        char charfirst = totalchar.charAt(indexfirst);

        // Characters from 2 until totallength
        // minimum 1 number & minimum 1 special char at random place inside the length
        StringBuilder builderChar = new StringBuilder();
        Random random = new Random();

        int totallength = (lengthchar - 1) + lengthnumber + lengthspecial;

        // define place of minimum 1 number  and minumum 1 specialcharacter ad random and not same place
        int charnumberindex = getRandomIndex(totallength) ;
        int charspecialindex= getRandomIndex(totallength) ;
        while (charnumberindex == charspecialindex){
            charspecialindex = getRandomIndex(totallength) ;
        }

        // construct of password from character 2 until totallength
        for (int i=0; i<totallength; i++){
            // generate randomnumber
            int index;

            char randomchar;
            // get char out table totalchar for the index
            if (i == charnumberindex) {
                index = random.nextInt(numberchar.length());
                randomchar = numberchar.charAt(index);
            } else {
                if (i == charspecialindex) {
                    index = random.nextInt(specialchar.length());
                    randomchar = specialchar.charAt(index);
                } else {
                    index = random.nextInt(allchar.length());
                    randomchar = allchar.charAt(index);
                }
            }
            // add char of the index to builder
            builderChar.append(randomchar);
        }

        return charfirst + builderChar.toString() ;

    }

    public static int getRandomIndex(int length){
        Random charnumber = new Random();
        int index = charnumber.nextInt(length);

        return index;
    }
}
