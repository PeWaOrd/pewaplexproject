package be.ordina;

import be.ordina.general.ChildUser;
import be.ordina.general.MainUser;
import be.ordina.pages.*;
import be.ordina.system.Site;
import com.tngtech.java.junit.dataprovider.DataProvider;
import com.tngtech.java.junit.dataprovider.DataProviderRunner;
import com.tngtech.java.junit.dataprovider.UseDataProvider;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.*;
import org.junit.runner.RunWith;
import org.openqa.selenium.WebDriver;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import static org.junit.Assert.assertTrue;

@RunWith(DataProviderRunner.class)
public class AppTest1Users {

    private static WebDriver driver;
    private static UsersandSharing usersandSharing;
    private static PlexApplication plexApplication;
    static File FileName = new File("TestDataFiles\\DataPlexTesten.xlsx");
    public static DataFormatter formatterAddusers = new DataFormatter();
    public static DataFormatter formatterDeleteusers = new DataFormatter();

    @BeforeClass
    public static void InitBeforeAllTests() throws InterruptedException {
        driver = Site.openPlexChrome(driver);
        SignInGeneralAndLaunch();
        PlexApplicationControl();
        UserandSharingControl();
    }

    @Before
    public void InitBeforeTest(){

    }


    @DataProvider
    public static Object[][] AddUsersData () throws IOException {
        FileInputStream file = new FileInputStream(FileName);
        XSSFWorkbook workbook = new XSSFWorkbook(file);
        XSSFSheet sheet = workbook.getSheet("AddUserTesten");

        int RowNum = sheet.getPhysicalNumberOfRows();
        int ColNum = sheet.getRow(0).getPhysicalNumberOfCells();

        System.out.println("Number of rows:" + RowNum + " number of columns: " + ColNum);
        String[][] xlDataAddUsers = new String[RowNum-1][ColNum-1];

        for (int i = 0; i < RowNum - 1; i++)
        {
            XSSFRow row = sheet.getRow(i + 1);
            for (int j = 0; j < ColNum-1; j++)
            {
                if (row == null)
                    xlDataAddUsers[i][j] = "";
                else {
                    XSSFCell cell = row.getCell(j);
                    if (cell == null)
                        xlDataAddUsers[i][j] = "";
                    else {
                        String value = formatterAddusers.formatCellValue(cell);
                        xlDataAddUsers[i][j] = value.trim();
                    }
                }
            }
        }
        return xlDataAddUsers;

    }

    @DataProvider
    public static Object[][] DeleteUsersData () throws IOException {
        FileInputStream file = new FileInputStream(FileName);
        XSSFWorkbook workbook = new XSSFWorkbook(file);
        XSSFSheet sheet = workbook.getSheet("DeleteUserTesten");

        int RowNum = sheet.getPhysicalNumberOfRows();
        int ColNum = sheet.getRow(0).getPhysicalNumberOfCells();

        System.out.println("Number of rows:" + RowNum + " number of columns: " + ColNum);
        String[][] xlDataDeleteUsers = new String[RowNum-1][ColNum-1];

        for (int i = 0; i < RowNum - 1; i++)
        {
            XSSFRow row = sheet.getRow(i + 1);
            for (int j = 0; j < ColNum-1; j++)
            {
                if (row == null)
                    xlDataDeleteUsers[i][j] = "";
                else {
                    XSSFCell cell = row.getCell(j);
                    if (cell == null)
                        xlDataDeleteUsers[i][j] = "";
                    else {
                        String value = formatterDeleteusers.formatCellValue(cell);
                        xlDataDeleteUsers[i][j] = value.trim();
                    }
                }
            }
        }
        return xlDataDeleteUsers;

    }


    @Test
    @UseDataProvider("AddUsersData")
    public void addUsers(String username, String restriction)  {
        ChildUser childuser = new ChildUser(username, restriction);
        System.out.println("Process User : " + childuser.getName() + " " + childuser.getProfile());
        int index = usersandSharing.CheckUser(childuser, "Addusers");
        usersandSharing.Adduser(childuser);

    }

    @Test
    @UseDataProvider("DeleteUsersData")
    public void deleteusers(String username) throws InterruptedException {
        ChildUser childuser = new ChildUser(username);
        System.out.println("Process User : " + childuser.getName());
        int index = usersandSharing.CheckUser(childuser, "Deleteusers");
        usersandSharing.Deleteuser(childuser, index);
    }

    @After
    public void FinalAfterTest(){

    }

    @AfterClass
    public static void FinalAfterAllTests(){
        plexApplication.menuLogout();
        driver.close();
        System.out.println("End of tests TestUsers");
    }

    // other methods
    public static void SignInGeneralAndLaunch() throws InterruptedException {
        HomePage homePage = new HomePage(driver);
        homePage.clickSignIn();

        SignInFrame signInframe = new SignInFrame(driver);
        MainUser mainuser = new MainUser("Manager429", "Manager429@mailinator.com", "J3tJ+K=sUf$4");

        signInframe.signInMainuser(mainuser);

        homePage.launch();
        homePage.selectUser();
    }

    public static void PlexApplicationControl(){
        plexApplication = new PlexApplication(driver);
        plexApplication.checkPlexOverview();

        plexApplication.menuUserandSharing();

    }

    public static void UserandSharingControl(){
        usersandSharing = new UsersandSharing(driver);
        usersandSharing.checkPage();
    }
}
