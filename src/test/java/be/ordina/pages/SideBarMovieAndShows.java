package be.ordina.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class SideBarMovieAndShows {
    private final WebDriver driver;
    private WebDriverWait wait;

    private By btntabs = By.xpath("//a[@data-qa-id='tabButton']");

    public SideBarMovieAndShows(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver,20);
    }

    public void selectTabCategorie(String tabname){
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(btntabs));

        List<WebElement> elementstab = driver.findElements(btntabs);
        for (int i=0;i<=elementstab.size();i++){
            String username = elementstab.get(i).getText();
            if (username.contains(tabname) && elementstab.get(i).isDisplayed()){
                elementstab.get(i).click();
                break;
            }
        }

    }

}
