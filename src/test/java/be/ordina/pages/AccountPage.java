package be.ordina.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static org.junit.Assert.assertTrue;

// Temp only used in AppTest for creating new accounts

public class AccountPage {
    private final WebDriver driver;
    private WebDriverWait wait;

    private By tbaccount = By.xpath("//input[@id='username']");
    private By txtaccount = By.xpath("//h2[@class='SettingsPageHeader-header-1ugtIL']");
    private By btnUserAccountEdit = By.xpath("//label[@class='SubmittableFormSection-label-1uVA_5 FormLabel-label-2aLJ9b CloseableFormSection-isPressable-3BPTnc ']");
    private By btnSaveAccountName = By.xpath("//button[@type='submit']");
    private By btnHome = By.xpath("//a[@aria-label='Home']");


    public AccountPage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver,20);
    }

    public void checkAccount(){
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(btnUserAccountEdit));
        assertTrue(driver.findElement(txtaccount).getText().contains("Account"));
        System.out.println("On account page");
    }

    public void changeUsername(String username) throws InterruptedException {

        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(btnUserAccountEdit));

        List<WebElement> elementschange = driver.findElements(btnUserAccountEdit);

        for(int i=0;i<= elementschange.size();i++){
            String typechange = elementschange.get(i).getText();

            if (typechange.contains("USERNAME")){
                System.out.println(typechange);
                elementschange.get(i).click();
         // With .clear and .sendkeys, the value in textfile didn't clear but added. Via Keys.chord it clear and put
         // value
                //       driver.findElement(tbaccount).sendKeys(username);

                driver.findElement(tbaccount).sendKeys(Keys.chord(Keys.CONTROL, "a"), username);
                driver.findElement(btnSaveAccountName).click();

                driver.findElement(btnHome).click();
                break;
            }
        }

    }
}
