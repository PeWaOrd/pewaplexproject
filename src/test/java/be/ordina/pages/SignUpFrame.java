package be.ordina.pages;

import be.ordina.general.MainUser;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;


public class SignUpFrame {
    private final WebDriver driver;
    private WebDriverWait wait;


    private String nameframe = "fedauth-iFrame";

    private By tbEmailAddress = By.xpath("//input[@id='email']");
    private By tbPassword = By.xpath("//input[@id='password']");
    private By btnCreateAccount = By.xpath("//button[@type='submit']");

    public SignUpFrame(WebDriver driver) {
       this.driver = driver;
       this.wait = new WebDriverWait(driver,20);

    }

    public MainUser signup(){
        wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("fedauth-iFrame"));
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(tbEmailAddress));

        MainUser MainUser = new MainUser();
        System.out.println(MainUser.getName());
        System.out.println(MainUser.getEmail());
        System.out.println(MainUser.getPassword());

        driver.findElement(tbEmailAddress).sendKeys(MainUser.getEmail());
        driver.findElement(tbPassword).sendKeys(MainUser.getPassword());
        driver.findElement(btnCreateAccount).click();

        return MainUser;
    }

}
