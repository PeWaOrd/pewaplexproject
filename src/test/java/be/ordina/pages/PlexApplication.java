package be.ordina.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static org.junit.Assert.assertTrue;

public class PlexApplication {
    private final WebDriver driver;
    private WebDriverWait wait;

    private By txthome = By.xpath("//span[@class='PageHeaderBreadcrumbButton-link-30Jgzu']");
    private By tbsearch = By.xpath("//input[@data-qa-id='quickSearchInput']");
    private By txtsearch = By.xpath("//a[@data-qa-id='quickSearchItemLink']");

    private By txtdetail = By.xpath("//div[@class='PrePlayLeftTitle-leftTitle-3RWvGy']");

    private By txtOnWatchlist = By.xpath("//span[@class='PrePlayPosterCardPlayedStatus-label-1a29LI']");
    private By btnPutOnWatchlist = By.xpath("//button[@data-qa-id='preplay-addToWatchlist']");
    private By btnRemoveOnWatchlist = By.xpath("//button[@data-qa-id='preplay-removeFromWatchlist']");
//    private By btnMenuList = By.xpath("//button[@id='id-10']");
    private By btnMenuList = By.xpath("//button[@class='NavBar-accountButton-3EEZiM NavBarAccountButton-button-1Y4ek6 DisclosureArrowButton-disclosureArrowButton-11LSbY Link-link-2n0yJn DisclosureArrowButton-medium-3zt1WM  Link-link-2n0yJn Link-default-2XA2bN     ']");
    private By btnAccount = By.xpath("//a[@href='#!/settings/account']");
    private By btnAction = By.xpath("//button[@data-qa-id='dropdownItem']");

    private By btnMenuScrollBarItem = By.xpath("//div[@class='SourceSidebarLink-title-1lgRT2 SidebarLink-title-2vaAAn']");

    public PlexApplication(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver,20);
        driver.switchTo().defaultContent();
    }

    public void checkPlexOverview(){
        wait.until(ExpectedConditions.visibilityOfElementLocated(txthome));

        assertTrue(driver.findElement(txthome).getText().contains("Home"));
        System.out.println("Overview page PlexApplication available");
    }

    public void menuUserandSharing(){
        wait.until(ExpectedConditions.presenceOfElementLocated(btnMenuList));
        driver.findElement(btnMenuList).click();

        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(btnAction));
        List<WebElement> elementsaction = driver.findElements(btnAction);

        for(int i=0;i<= elementsaction.size();i++) {
            String typeaction = elementsaction.get(i).getText();

            if (typeaction.contains("Users & Sharing")) {
                System.out.println(typeaction);
                elementsaction.get(i).click();
                break;
            }
        }
    }

    public void menuSwitchUser(){
        wait.until(ExpectedConditions.presenceOfElementLocated(btnMenuList));
        driver.findElement(btnMenuList).click();

        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(btnAction));
        List<WebElement> elementsaction = driver.findElements(btnAction);

        for(int i=0;i<= elementsaction.size();i++) {
            String typeaction = elementsaction.get(i).getText();

            if (typeaction.contains("Switch User...")) {
                System.out.println(typeaction);
                elementsaction.get(i).click();
                break;
            }
        }
    }


    public void menuLogout(){
        wait.until(ExpectedConditions.presenceOfElementLocated(btnMenuList));
        driver.findElement(btnMenuList).click();

        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(btnAction));
        List<WebElement> elementsaction = driver.findElements(btnAction);

        for(int i=0;i<= elementsaction.size();i++) {
            String typeaction = elementsaction.get(i).getText();

            if (typeaction.contains("Sign Out")) {
                System.out.println(typeaction);
                elementsaction.get(i).click();
                break;
            }
        }
    }

    public void sideBarMovieAndShows(){
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(btnMenuScrollBarItem));

        List<WebElement> elementssidebar = driver.findElements(btnMenuScrollBarItem);

        for(int i=0;i<= elementssidebar.size();i++) {
            String sidebaraction = elementssidebar.get(i).getText();

            if (sidebaraction.contains("Movies & Shows")) {
                System.out.println(sidebaraction);
                elementssidebar.get(i).click();
                break;
            }
        }

    }

// History used by the first test in AppTest
    public void menuNameUser(){
        driver.findElement(btnMenuList).click();
        driver.findElement(btnAccount).click();
    }

    public void checkPlexDetail(String checkmovie){
        wait.until(ExpectedConditions.visibilityOfElementLocated(txtdetail));

        assertTrue(driver.findElement(txtdetail).getText().contains(checkmovie));
        System.out.println("Detail page movie " + checkmovie + " available");
    }

    public void searchMovie(String searchmovie){
        driver.findElement(tbsearch).sendKeys(searchmovie);
 
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(txtsearch));
        List<WebElement> elementstitles = driver.findElements(txtsearch);
        System.out.println("Found movies " + elementstitles.size());

        for (int i=0;i<= elementstitles.size(); i++){
            String textmovietitle = elementstitles.get(i).getAttribute("title");

            if (textmovietitle.contentEquals(searchmovie)){
                elementstitles.get(i).click();
                break;
            }
        }
    }

    public void watchListHandle(){
        if (driver.findElements(txtOnWatchlist).isEmpty()){
            System.out.println("Movie not yet on watchlist, add on watchlist ");
            wait.until(ExpectedConditions.elementToBeClickable(btnPutOnWatchlist));
            driver.findElement(btnPutOnWatchlist).click();
        }
        else {
            System.out.println("Movie already on wachtlist");
  //          wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(btnRemoveOnWatchlist));
  //          driver.findElement(btnRemoveOnWatchlist).click();
        }
    }

}
