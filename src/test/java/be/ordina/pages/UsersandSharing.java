package be.ordina.pages;
import be.ordina.general.ChildUser;

import static java.lang.Thread.sleep;


import org.openqa.selenium.By;
import org.openqa.selenium.TimeoutException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class UsersandSharing {
    private final WebDriver driver;
    private WebDriverWait wait;

    private By txtUsersandSharing = By.xpath("//h2[@class='SettingsPageHeader-header-1ugtIL']");
    private By btnCreateUser = By.xpath("//button[@class='UsersSettingsPage-button-1upQLF SpinnerButton-button-1A8EcL Button-button-2kT68l Link-link-2n0yJn  Button-button-2kT68l Link-link-2n0yJn Button-default--yDCH5 Button-small-PnnoGC Link-link-2n0yJn Link-default-2XA2bN     ']");

    private By tbusername = By.xpath("//input[@id='username']");
    private By tbrestriction = By.xpath("//select[@id='restrictionProfileIdentifier']");

    private By btncancel = By.xpath("//button[@class=' Button-button-2kT68l Link-link-2n0yJn Button-default--yDCH5 Button-medium-3g45_Q Link-link-2n0yJn Link-default-2XA2bN     ']");
    private By btncontinue = By.xpath("//button[@type='submit']");
    private By btnadd = By.xpath("//button[@class=' SpinnerButton-button-1A8EcL Button-button-2kT68l Link-link-2n0yJn  Button-button-2kT68l Link-link-2n0yJn Button-primary-3fwLzo Button-medium-3g45_Q Link-link-2n0yJn Link-default-2XA2bN     ']");
    private By btnskip = By.xpath("//button[@class='skip-btn btn-gray']");

    private By txtExistingUsers = By.xpath(("//div[@class='UsersSettingsRowName-title-kkZcaJ']"));
    private By btndeleteuser = By.xpath("//button[@aria-label='Remove From Plex Home']");
    private By btnconfirmdelete = By.xpath("//button[@class='ConfirmModal-confirmButton-1DVQ-r SpinnerButton-button-1A8EcL Button-button-2kT68l Link-link-2n0yJn  Button-button-2kT68l Link-link-2n0yJn Button-danger-2T8IUN Button-medium-3g45_Q Link-link-2n0yJn Link-default-2XA2bN     ']");

    public UsersandSharing(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver,2);
    }

    public void checkPage(){
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(btnCreateUser));
        assertTrue(driver.findElement(txtUsersandSharing).getText().contains("Users & Sharing"));
        System.out.println("On Users & Sharing page");
    }

    public int CheckUser(ChildUser childUser, String source){
        List<WebElement> existingusers = driver.findElements(txtExistingUsers);
        boolean userexists = false;
        int i = 0;
        for (WebElement user : existingusers){
            if (user.getText().equals(childUser.getName())) {
                userexists = true;
                break;
            }
            else {
                i++;
            }
        }

        switch (source) {
            case "Addusers":
                // userexists must be false, when false no error shown, when true error is shown
                assertFalse("User '" + childUser.getName() + "' already exists", userexists);
                break;
            case "Deleteusers":
                // userexusts must be true, when true no error, when false error is shown
                assertTrue("User '" + childUser.getName() + "' doesn't exist for delete" , userexists);
                break;
            default:
                System.out.println("Not a correct source for checkusers");
        }

        return i;
    }

    public void Deleteuser(ChildUser childUser, int index){
        List<WebElement> deleteuser = driver.findElements(btndeleteuser);
        System.out.println(deleteuser.size());

        deleteuser.get(index - 1).click();

        wait.until(ExpectedConditions.presenceOfElementLocated(btnconfirmdelete));
        wait.until(ExpectedConditions.elementToBeClickable(btnconfirmdelete));
        driver.findElement(btnconfirmdelete).click();

    }

    public void Adduser(ChildUser childuser) {
        wait.until(ExpectedConditions.presenceOfElementLocated(btnCreateUser));
        wait.until(ExpectedConditions.elementToBeClickable(btnCreateUser));
        driver.findElement(btnCreateUser).click();

        // add waits
        wait.until(ExpectedConditions.presenceOfElementLocated(tbusername));
        driver.findElement(tbusername).sendKeys(childuser.getName());
        Select restrictions = new Select(driver.findElement(tbrestriction));

        List<WebElement> dd = restrictions.getOptions();

        // Search if restriction exists in list of possibilities
        List<WebElement> options = restrictions.getOptions();
        boolean optionExists = false;
        for (WebElement e : options) {
            if (e.getText().equals(childuser.getProfile())) {
                optionExists = true;
                break;
            }
        }

        // if restriction exists, then add user
        if (optionExists) {
            restrictions.selectByVisibleText(childuser.getProfile());
            driver.findElement(btncontinue).click();

            wait.until(ExpectedConditions.presenceOfElementLocated(btnadd));
            wait.until(ExpectedConditions.elementToBeClickable(btnadd));
            driver.findElement(btnadd).click();

            // treatment of pincode, define no pincode
            try {
                wait.until(ExpectedConditions.visibilityOfElementLocated(btnskip));
                List<WebElement> elementsbtnpin = driver.findElements(btnskip);
                if (elementsbtnpin.size() > 00 && elementsbtnpin.get(0).isDisplayed()) {
                    driver.findElement(btnskip).click();
                }
            } catch (TimeoutException e) {
                boolean nopincode = false;
            }
        }
        // restrictions doesn't exists, cancel  user
        else {
            driver.findElement(btncancel).click();
        }

        //
        assertTrue("Restriction '" + childuser.getProfile() + "' doesn't exist", optionExists);

    }

}
