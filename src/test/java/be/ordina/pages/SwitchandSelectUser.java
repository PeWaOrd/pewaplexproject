package be.ordina.pages;

import be.ordina.general.User;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

public class SwitchandSelectUser {
    private final WebDriver driver;
    private WebDriverWait wait;

    private By btnuser = By.xpath("//div[@class='username']");

    public SwitchandSelectUser(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver,20);
    }

    public void SelectUser(User user){
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(btnuser));

        List<WebElement> elementsuser = driver.findElements(btnuser);
        for (int i=0;i<=elementsuser.size();i++){
            String username = elementsuser.get(i).getText();
            if (username.contains(user.getName()) && elementsuser.get(i).isDisplayed()){
                elementsuser.get(i).click();
                break;
            }
        }

    }
}
