package be.ordina.pages;

import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static java.lang.Thread.sleep;
import static org.junit.Assert.assertTrue;

public class HomePage {
    private final WebDriver driver;
    private WebDriverWait wait;

    private By btnSignup = By.xpath("//a[@class='signup button']");
    private By btnSignin = By.xpath("//a[@class='signin']");
    private By btnlaunch = By.xpath("//a[@class='launch button']");

    private By btnuser = By.xpath("//div[@class='username']");


    public HomePage(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver,5);
    }

    public void clickSignUp(){
        driver.findElement(btnSignup).click();
    }

    public void clickSignIn(){
        driver.findElement(btnSignin).click();
    }


    public void launch() throws InterruptedException {
        driver.switchTo().defaultContent();
        wait.until(ExpectedConditions.visibilityOfElementLocated(btnlaunch));

        assertTrue(driver.findElement(btnlaunch).getText().contentEquals("Launch"));
        driver.findElement(btnlaunch).click();
    }

    public void selectUser(){
        try {
            wait.until(ExpectedConditions.visibilityOfAllElementsLocatedBy(btnuser));
            List<WebElement> elementsbtnmainuser = driver.findElements(btnuser);
            if (elementsbtnmainuser.size() > 00 && elementsbtnmainuser.get(0).isDisplayed()) {
                driver.findElement(btnuser).click();
            }
        } catch (TimeoutException e){
            driver.navigate().refresh();
        }


    }

}
