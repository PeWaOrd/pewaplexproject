package be.ordina.pages;

import be.ordina.general.ChildUser;
import be.ordina.general.MainUser;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class SignInFrame {
    private final WebDriver driver;
    private WebDriverWait wait;

    private By tbEmailAddress = By.xpath("//input[@id='email']");
    private By tbPassword = By.xpath("//input[@id='password']");
    private By btnConfirm = By.xpath("//button[@type='submit']");


    public SignInFrame(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver, 20);
    }

    public void signIn(String username, String email, String password) {
        wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("fedauth-iFrame"));
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(tbEmailAddress));

        MainUser user = new MainUser(username, email, password);

     // Input field EmailAddress: an e-mail of user can be used. For the test, we use the username
     // driver.findElement(tbEmailAddress).sendKeys(user.getEmail());
        driver.findElement(tbEmailAddress).sendKeys(user.getName());
        driver.findElement(tbPassword).sendKeys(user.getPassword());
        driver.findElement(btnConfirm).click();

    }

    public void signInMainuser(MainUser mainuser) {
        wait.until(ExpectedConditions.frameToBeAvailableAndSwitchToIt("fedauth-iFrame"));
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(tbEmailAddress));


        // Input field EmailAddress: an e-mail of user can be used. For the test, we use the username
        // driver.findElement(tbEmailAddress).sendKeys(user.getEmail());
        driver.findElement(tbEmailAddress).sendKeys(mainuser.getName());
        driver.findElement(tbPassword).sendKeys(mainuser.getPassword());
        driver.findElement(btnConfirm).click();

    }

}
