package be.ordina.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

import java.util.List;

import static org.junit.Assert.assertFalse;
import static org.junit.Assert.assertTrue;

public class Categories {
    private final WebDriver driver;
    private WebDriverWait wait;
    private String categoriename;
    private String nrallowedmovies;

    // change in the view of categorie
//    private By btncategorie = By.xpath("//a[@class=' MetadataPosterTitle-singleLineTitle-24_DNu MetadataPosterTitle-title-3tU5F9   Link-link-2n0yJn Link-default-2XA2bN     ']");
    private By btncategorie = By.xpath("//a[@class='MetadataDirectoryPosterCard-overlayLink-3Lb0W4 MetadataPosterCardOverlay-link-1SwhlG Link-link-2n0yJn Link-default-2XA2bN     ']");
    private By txtcategorietitle = By.xpath("//span[@class='MetadataDirectoryPosterCell-title-3smW-W']");
    private By txtcategoriename = By.xpath("//span[@class='PageHeaderBreadcrumbButton-link-30Jgzu']");
    private By txtnumbermovies = By.xpath("//span[@class='PageHeaderBadge-badge-2oDBgn Badge-badge-21NQRi Badge-default-VTVet2 Badge-isHidden-2p8gjS' or @class='PageHeaderBadge-badge-2oDBgn Badge-badge-21NQRi Badge-default-VTVet2 ']");

    public Categories(WebDriver driver) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver,20);
    }

    public Categories(WebDriver driver, String categoriename, String nrallowedmovies) {
        this.driver = driver;
        this.wait = new WebDriverWait(driver,20);
        this.categoriename = categoriename;
        this.nrallowedmovies = nrallowedmovies;
    }

    public String getCategoriename() {
        return categoriename;
    }

    public void setCategoriename(String categoriename) {
        this.categoriename = categoriename;
    }

    public String getNrallowedmovies() {
        return nrallowedmovies;
    }

    public void setNrallowedmovies(String nrallowedmovies) {
        this.nrallowedmovies = nrallowedmovies;
    }


    public void CheckOnCategories(){
        wait.until(ExpectedConditions.presenceOfElementLocated(txtcategoriename));
        assertTrue(driver.findElement(txtcategoriename).getText().contains("Categories"));
        System.out.println("Overview page Categories available");


    }
    public void SelectCategorie(){
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(btncategorie));
        wait.until(ExpectedConditions.presenceOfAllElementsLocatedBy(txtcategorietitle));

        List<WebElement> elementscategorietitle = driver.findElements(txtcategorietitle);
        List<WebElement> elementscategoriebtn = driver.findElements(btncategorie);

        for (int i=0;i<=elementscategorietitle.size();i++){
            String categoriename = elementscategorietitle.get(i).getText();
            if (categoriename.contains(getCategoriename()) && elementscategorietitle.get(i).isDisplayed() &&
                elementscategoriebtn.get(i).isDisplayed()){
                elementscategoriebtn.get(i).click();
                break;
            }
        }

    }

    public void CheckAllowedCategorie(){
        boolean checkallowedNrmovies = false;

        wait.until(ExpectedConditions.presenceOfElementLocated(txtcategoriename));
        assertTrue(driver.findElement(txtcategoriename).getText().contains(getCategoriename()));

        int numbermoviesshown = Integer.parseInt(driver.findElement(txtnumbermovies).getText());
        System.out.println(numbermoviesshown);

        if (getNrallowedmovies().contains("unlimited"))
        {
            checkallowedNrmovies = true;
        } else {
            int numbermoviesallowed = Integer.parseInt(getNrallowedmovies());
            if (numbermoviesshown <= numbermoviesallowed) {
                checkallowedNrmovies = true;
            }
        }

        // checkallowedNrmoview bust be true, when true no error, when false error is shown
        assertTrue("Number of movies " + numbermoviesshown + " available for " + getCategoriename() + "is not allowed (max " + getNrallowedmovies() + ")", checkallowedNrmovies);

    }
}
